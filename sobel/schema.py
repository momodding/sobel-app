from datetime import datetime
from typing import List, Union
from pydantic import BaseModel

class ClassificationBaseSchema(BaseModel):
    id: int
    filename: Union[str, None] = None
    saved_filename: Union[str, None] = None
    classes: Union[str, None] = None
    created_at: Union[datetime, None] = None

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        arbitrary_types_allowed = True