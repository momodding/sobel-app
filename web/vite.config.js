import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import { BootstrapVueNextResolver } from 'unplugin-vue-components/resolvers';
import Components from 'unplugin-vue-components/vite';
import { defineConfig } from 'vite';
import VueDevTools from 'vite-plugin-vue-devtools';

const projectRootDir = resolve(__dirname);
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    VueDevTools(),
    vue(),
    Components({
      resolvers: [BootstrapVueNextResolver()],
    })
  ],
  resolve: {
    alias: {
      "@": resolve(projectRootDir, "src"),
    },
  },
  server: {
    watch: {
      usePolling: true
    }
  }
})
