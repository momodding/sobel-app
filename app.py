import time
from typing import Any
from fastapi.staticfiles import StaticFiles
from fastapi_pagination import Page, add_pagination, paginate
from slugify import slugify
from sqlalchemy import desc
from sobel import models
from sobel.database import get_db, engine
from sobel.models import Classification
from sobel.schema import ClassificationBaseSchema
import sobel_engine
from fastapi import Depends, FastAPI, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session
from fastapi_pagination.utils import disable_installed_extensions_check

models.Base.metadata.create_all(bind=engine)
app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:5173",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/api/roadhole")
def read_root():
    return {"Hello": "World"}


@app.post("/api/roadhole/classify")
async def read_item(file: UploadFile, db: Session = Depends(get_db)):
    contents = file.file.read()
    sobel_service = sobel_engine.SobelService()
    image = sobel_service.load_image_from_binary(contents)
    gray_image = sobel_service.convert_to_grayscale(image)
    median_filtered_image = sobel_service.apply_median_filter(gray_image)
    sobel_edge = sobel_service.sobel_edge_detection(median_filtered_image)
    BW = sobel_service.convert_to_black_and_white(sobel_edge)
    closing = sobel_service.perform_closing_operation(BW)
    expanded_edges = sobel_service.expand_edges(closing)
    filled_holes = sobel_service.fill_holes(expanded_edges, image)
    opening = sobel_service.perform_opening_operation(filled_holes)
    erosion = sobel_service.perform_erosion(opening)
    base64img, damage_levels = sobel_service.countour_detection(image, erosion)

    seconds = time.time()
    level = slugify(damage_levels[0])
    saved_filename = f"{int(seconds)}_{level}.png"
    sobel_service.save_to_dist_file(f"{int(seconds)}_{level}", base64img)

    classification = Classification(filename=file.filename, saved_filename=saved_filename, classes=damage_levels[0])
    db.add(classification)
    db.commit()

    orig_file = sobel_service.upload_to_base64_file(contents)

    return {"file": base64img, "orig_file": orig_file, "kerusakan":damage_levels}


@app.get("/api/roadhole/class/list")
async def get_class_list():
    class_list = [
        {"Tingkat Rendah":"Kerusakan Kecil"},
        {"Tingkat Sedang":"Kerusakan Sedang"},
        {"Tingkat Tinggi":"Kerusakan Tinggi"},
    ]

    return {"class_list": class_list}

@app.get("/api/roadhole/image/list")
def get_image_list(damage_level: str = None, db: Session = Depends(get_db)) -> Page[ClassificationBaseSchema]:
    classification = db.query(Classification)
    if (damage_level is not None and damage_level):
        classification = classification.filter_by(classes=damage_level)

    classification = classification.order_by(desc(Classification.created_at)).all()

    return paginate(classification)

disable_installed_extensions_check()
add_pagination(app)
app.mount('/', StaticFiles(directory='./web/dist', html=True))