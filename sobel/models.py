from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, TIMESTAMP, func
from sqlalchemy.orm import relationship
from fastapi_utils.guid_type import GUID, GUID_DEFAULT_SQLITE
from .database import Base


class Classification(Base):
    __tablename__ = "classification"

    id = Column(Integer, primary_key=True, index=True)
    filename = Column(String, nullable=True)
    saved_filename = Column(String, nullable=True)
    classes = Column(String, nullable=True)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
