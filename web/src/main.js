import router from '@/router'
import axios from 'axios'
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css'
import 'bootstrap/dist/css/bootstrap.css'
import { createApp } from 'vue'
import VueAxios from 'vue-axios'
import App from './App.vue'
// import './style.css'

const app = createApp(App).use(router)
app.use(VueAxios, axios)
app.provide('axios', app.config.globalProperties.axios)
app.mount('#app')
